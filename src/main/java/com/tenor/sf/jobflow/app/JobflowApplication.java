package com.tenor.sf.jobflow.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class JobflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobflowApplication.class, args);
	}
}
